/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    RPNCalculatorTest.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 * @brief   Circle CPPUNIT test implementation. DO NOT MODIFY THE CONTENTS OF THIS FILE!
 * @see     http://sourceforge.net/projects/cppunit/files
 */

#include <sstream>
#include "RPNCalculatorTest.h"
#include "../main/RPNCalculator.h"

CPPUNIT_TEST_SUITE_REGISTRATION(RPNCalculatorTest);

RPNCalculatorTest::RPNCalculatorTest() {
    /* no-op */
}

void RPNCalculatorTest::setUp() {
    /* no-op */
}

void RPNCalculatorTest::tearDown() {
    /* no-op */
}

void testItWith(std::string expression, int expected) {
    csc232::RPNCalculator calculator;
    int actual = calculator.evaluate(expression);
    std::stringstream ss;
    ss << "Problem: I expected " << expected << " but the expression \""
       << expression << "\" evaluated to " << actual << "...";
    CPPUNIT_ASSERT_EQUAL_MESSAGE(ss.str(), expected, actual);
}

void RPNCalculatorTest::testSimpleAddition() {
    testItWith("1 2 +", 3);
}

void RPNCalculatorTest::testSimpleSubtraction() {
    testItWith("2 1 -", 1);
}

void RPNCalculatorTest::testSimpleMultiplication() {
    testItWith("3 4 *", 12);
}

void RPNCalculatorTest::testSimpleDivision() {
    testItWith("12 4 /", 3);
}

void RPNCalculatorTest::testTwoOperations() {
    testItWith("2 3 4 + *", 14);
}

void RPNCalculatorTest::testExpressionWithLeadingWhiteSpace() {
    testItWith("     2 3 4 + *", 14);
}

void RPNCalculatorTest::testExpressionWithTrailingWhiteSpace() {
    testItWith("2 3 4 + *     ", 14);
}

void RPNCalculatorTest::testExpressionWithEmbeddedTabs() {
    testItWith("2\t3\t4\t+\t*", 14);
}

void RPNCalculatorTest::testExpressionWithEmbeddedNewLine() {
    testItWith("2\n3\n4\n+\n*\n", 14);
}

void RPNCalculatorTest::testExpressionWithMixedWhiteSpace() {
    testItWith("\n2\t 3\t 4\n +\n *\n", 14);
}