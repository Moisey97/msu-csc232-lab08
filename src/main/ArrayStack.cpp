/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file    ArrayStack.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 * @brief   Definition for an array-based implementation of the Stack interface.
 */

#include "ArrayStack.h"

namespace csc232 {
    template<typename T>
    ArrayStack<T>::ArrayStack() : top{-1} {
        /* no-op */
    }

    template<typename T>
    bool ArrayStack<T>::isEmpty() const {
        return top < 0;
    }

    template<typename T>
    bool ArrayStack<T>::push(const T &newEntry) throw(PrecondViolatedExcept) {
        bool result{false};
        if (top < DEFAULT_CAPACITY - 1) { // Does the stack have room for newEntry?
            ++top;
            items[top] = newEntry;
            result = true;
        } else {
            throw(PrecondViolatedExcept("push() called on full stack"));
        }
        return result;
    }

    template<typename T>
    bool ArrayStack<T>::pop() {
        bool result{false};
        if (!isEmpty()) {
            --top;
            result = true;
        }
        return result;
    }

    template<typename T>
    T ArrayStack<T>::peek() const throw(PrecondViolatedExcept) {
        // Enforce precondition
        if (isEmpty()) {
            const std::string message{"peek() called with empty stack"};
            throw PrecondViolatedExcept(message);
        } else {
            return items[top];
        }
    }
}