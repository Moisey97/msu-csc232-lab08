/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file    PrecondViolatedExcept.h
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 * @brief   Specification for our custom pre-condition violated exception.
 */

#ifndef LAB08_PRECONDVIOLATEDEXCEPT_H
#define LAB08_PRECONDVIOLATEDEXCEPT_H

#include <stdexcept>

namespace csc232 {
    class PrecondViolatedExcept : public std::logic_error {
    public:
        PrecondViolatedExcept(const std::string &message = "");
    };
}

#endif //LAB08_PRECONDVIOLATEDEXCEPT_H
