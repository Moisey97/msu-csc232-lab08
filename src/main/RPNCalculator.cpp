/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file RPNCalculator.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <yname@missouristate.edu>
 *          Partner Name <pname@missouristate.edu>
 * @brief   Definition of RPNCalculator.
 */

#include "RPNCalculator.h"

namespace csc232 {
    int RPNCalculator::evaluate(const std::string &postfixExpress) {
        // TODO: Implement me second and return the appropriate value
        return 0;
    } // end RPNCalculator::evaluate(const std::string&)

    bool RPNCalculator::isOperand(const char op) const {
        // TODO: Implement me first and return the appropriate value
        return false;
    } // end RPNCalculator::isOperand(const char) const
} // end namespace csc232